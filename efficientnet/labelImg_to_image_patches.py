import os
import argparse
import time
import csv

import torch
import torch.nn.functional as F
import torch.utils.data as Data
import torch.nn as nn
from torchvision import transforms, models
import numpy as np
from PIL import Image
import matplotlib.pyplot as plt
import cv2


supported_cv2_formats = (".bmp", ".dib", ".jpeg", ".jpg", ".jpe", ".jp2", ".png", ".pbm", ".pgm", ".ppm", ".sr", ".ras", ".tiff", ".tif")


def list_images(imagedir):
    if os.path.isdir(imagedir):
        all_files = os.listdir(imagedir)
        images = [os.path.join(imagedir, x) for x in all_files if x.lower().endswith(supported_cv2_formats)]
        images.sort()
    return images


def make_box_img(img, annotation_file):
    width, height = image.size
    box_img = np.zeros((height, width)).astype(np.uint8)
    
    with open(annotation_file, 'r') as fp:
        for line in fp:
            if len(line.split(" ")) == 5:
                x = float(line.split(" ")[1])
                y = float(line.split(" ")[2])
                w = float(line.split(" ")[3])
                hh = line.split(" ")[4]
                h = float(hh.split("\n")[0])
                
                xx = x * width
                yy = y * height
                ww = w * width
                hh = h * height
                
                tlc = (int(xx - (0.5*ww)), int(yy - (0.5*hh)))
                brc = (int(xx + (0.5*ww)), int(yy + (0.5*hh)))
                box_img = cv2.rectangle(box_img, tlc, brc, (255), -1)

    box_img = np.expand_dims(box_img, axis=2)
    box_img = np.repeat(box_img, 3, axis=2)
    box_img = cv2.cvtColor(box_img, cv2.COLOR_BGR2RGB)

    return Image.fromarray(box_img)


def visualize_box(img, annotation_file):
    width, height = image.size
    img = cv2.cvtColor(np.array(img), cv2.COLOR_RGB2BGR)
    img_vis = img.copy()
    
    with open(annotation_file, 'r') as fp:
        for line in fp:
            if len(line.split(" ")) == 5:
                x = float(line.split(" ")[1])
                y = float(line.split(" ")[2])
                w = float(line.split(" ")[3])
                hh = line.split(" ")[4]
                h = float(hh.split("\n")[0])
                
                xx = x * width
                yy = y * height
                ww = w * width
                hh = h * height
                
                tlc = (int(xx - (0.5*ww)), int(yy - (0.5*hh)))
                brc = (int(xx + (0.5*ww)), int(yy + (0.5*hh)))
                img_vis = cv2.rectangle(img_vis, tlc, brc, (0, 0, 255), 10)

    img_vis = cv2.cvtColor(img_vis, cv2.COLOR_BGR2RGB)

    return Image.fromarray(img_vis)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--patches-5', action='store_true', help='whether to split the image into five smaller patches and include them in the training')
    parser.add_argument('--patches-10', action='store_true', help='whether to split the image into ten smaller patches and include them in the training')
    parser.add_argument('--classes', nargs="+", default=[], help='a list with all the classes')
    parser.add_argument('--img-size', type=int, default=545, help='the input image-size for the efficientnet-model')
    parser.add_argument('--max-img-width', type=int, default=1600, help='the desired image-width to transform to, if the original image-width is larger')
    parser.add_argument('--max-img-height', type=int, default=1200, help='the desired image-height to transform to, if the original image-height is larger')
    parser.add_argument('--view-width', type=int, default=800, help='the desired width for visualization')
    parser.add_argument('--view-height', type=int, default=800, help='the desired height for visualization')
    parser.add_argument('--img-folder', type=str, default='./images', help='the foldername that include the images in the same folder structure as the classes')
    parser.add_argument('--csv-file', type=str, default='./images/patches.csv', help='The output csv-file (full-path)')

    args = parser.parse_args()

    images = list_images(args.img_folder)
    breakout = False
    class_ids = np.arange(len(args.classes))
    csv_header = ['image_id']

    if args.patches_5:
        for n in range(5):
            csv_header.append('patch_' + str(n+1)) 

        ## the x in the lambda functions below represents the PIL-image
        patch_transform = transforms.Compose([transforms.Lambda(lambda x: transforms.Resize(int(round(args.img_size * 1.1)))(x) if min(x.size) < int(round(args.img_size * 1.1)) else x),
                                            transforms.Lambda(lambda x: transforms.Resize((args.max_img_height, args.max_img_width))(x) if x.size[0] > x.size[1] and (x.size[0] > args.max_img_width or x.size[1] > args.max_img_height) else x),
                                            transforms.Lambda(lambda x: transforms.Resize((args.max_img_width, args.max_img_height))(x) if x.size[1] > x.size[0] and (x.size[0] > args.max_img_width or x.size[1] > args.max_img_height) else x),
                                            transforms.FiveCrop((args.img_size, args.img_size)),
                                            ])

        
    
    if args.patches_10:
        for n in range(10):
            csv_header.append('patch_' + str(n+1)) 

        ## the x in the lambda functions below represents the PIL-image
        patch_transform = transforms.Compose([transforms.Lambda(lambda x: transforms.Resize(int(round(args.img_size * 1.1)))(x) if min(x.size) < int(round(args.img_size * 1.1)) else x),
                                            transforms.Lambda(lambda x: transforms.Resize((args.max_img_height, args.max_img_width))(x) if x.size[0] > x.size[1] and (x.size[0] > args.max_img_width or x.size[1] > args.max_img_height) else x),
                                            transforms.Lambda(lambda x: transforms.Resize((args.max_img_width, args.max_img_height))(x) if x.size[1] > x.size[0] and (x.size[0] > args.max_img_width or x.size[1] > args.max_img_height) else x),
                                            transforms.TenCrop((args.img_size, args.img_size)),
                                            ])


    annotated_images = []
    if not os.path.isfile(args.csv_file):
        with open(args.csv_file, 'w', newline='') as csvfile:
            csvwriter = csv.writer(csvfile, delimiter=',',quotechar='|', quoting=csv.QUOTE_MINIMAL)
            csvwriter.writerow(csv_header)
    else:
        with open(args.csv_file, 'r', newline='') as csvfile:
            csv_reader = csv.reader(csvfile)
            next(csv_reader)
            for row in csv_reader:
                annotated_images.append(row[0])


    title_part = "press "
    for p in range(1, len(class_ids)+1):
        title_part += "{0:d} for {1:s}, ".format(p, args.classes[class_ids[p-1]])
    title_part += "and ESC to exit program"

    for j in range(len(images)):
        list = []
        full_name = images[j]
        file_name = os.path.basename(full_name)
        annotation_file = os.path.splitext(file_name)[0] + ".txt"

        if file_name not in annotated_images:
            print(file_name)
            title = file_name + ' - ' + title_part
            image = Image.open(full_name).convert('RGB')
            if os.path.isfile(os.path.join(args.img_folder, annotation_file)):
                image = visualize_box(image, os.path.join(args.img_folder, annotation_file)) 
            patches = patch_transform(image)

            if os.path.isfile(os.path.join(args.img_folder, annotation_file)):
                box_img = make_box_img(image, os.path.join(args.img_folder, annotation_file))
                box_patches = patch_transform(box_img)
            else:
                box_patches = []

            writeline = [file_name]

            for h in range(len(patches)):
                patch = patches[h]
                npa = np.array(patch) 
                img = npa[:, :, ::-1] # Convert RGB to BGR
                resized = cv2.resize(img, (args.view_width, args.view_height), interpolation = cv2.INTER_AREA)
                n_white_pixels = 0

                try:
                    box_patch = box_patches[h]
                    npbb = np.array(box_patch)
                    img_box = npbb[:, :, ::-1]
                    n_white_pixels = len(np.where(img_box == (255, 255, 255))[0])
                except:
                    pass

                if n_white_pixels > 0:
                    remainder = args.classes.copy()
                    remainder.remove('healthy')
                    class_annot = remainder[0]
                    print("Patch {0:d}: {1:s}".format(h+1, class_annot))
                    writeline.append(class_annot)
                else:
                    class_annot = 'healthy'
                    print("Patch {0:d}: {1:s}".format(h+1, class_annot))
                    writeline.append(class_annot)

                cv2.imshow(title, resized)
                cv2.moveWindow(title, 0, 0)
                k = cv2.waitKey(1)

                if k == 27:
                    print('closing program')
                    breakout = True
                    break

            with open(args.csv_file, 'a', newline='') as csvfile:
                csvwriter = csv.writer(csvfile, delimiter=',',quotechar='|', quoting=csv.QUOTE_MINIMAL)
                csvwriter.writerow(writeline)
    
        cv2.destroyAllWindows() 
        if breakout:
            break

    print("Finished!")