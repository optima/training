import os
import argparse
import time
import csv

import torch
import torch.nn.functional as F
import torch.utils.data as Data
import torch.nn as nn
from torchvision import transforms, models
import numpy as np
from PIL import Image
import matplotlib.pyplot as plt
import cv2


supported_cv2_formats = (".bmp", ".dib", ".jpeg", ".jpg", ".jpe", ".jp2", ".png", ".pbm", ".pgm", ".ppm", ".sr", ".ras", ".tiff", ".tif")


def list_images(imagedir):
    if os.path.isdir(imagedir):
        all_files = os.listdir(imagedir)
        images = [os.path.join(imagedir, x) for x in all_files if x.lower().endswith(supported_cv2_formats)]
        images.sort()
    return images


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--patches-5', action='store_true', help='whether to split the image into five smaller patches and include them in the training')
    parser.add_argument('--patches-10', action='store_true', help='whether to split the image into ten smaller patches and include them in the training')
    parser.add_argument('--classes', nargs="+", default=[], help='a list with all the classes')
    parser.add_argument('--img-size', type=int, default=545, help='the input image-size for the efficientnet-model')
    parser.add_argument('--max-img-width', type=int, default=1600, help='the desired image-width to transform to, if the original image-width is larger')
    parser.add_argument('--max-img-height', type=int, default=1200, help='the desired image-height to transform to, if the original image-height is larger')
    parser.add_argument('--view-width', type=int, default=900, help='the desired width for visualization')
    parser.add_argument('--view-height', type=int, default=900, help='the desired height for visualization')
    parser.add_argument('--img-folder', type=str, default='./images', help='the foldername that include the images in the same folder structure as the classes')
    parser.add_argument('--csv-file', type=str, default='./images/patches.csv', help='The output csv-file (full-path)')

    args = parser.parse_args()

    images = list_images(args.img_folder)
    breakout = False
    class_ids = np.arange(len(args.classes))
    csv_header = ['image_id']

    if args.patches_5:
        for n in range(5):
            csv_header.append('patch_' + str(n+1)) 

        ## the x in the lambda functions below represents the PIL-image
        patch_transform = transforms.Compose([transforms.Lambda(lambda x: transforms.Resize(int(round(args.img_size * 1.1)))(x) if min(x.size) < int(round(args.img_size * 1.1)) else x),
                                            transforms.Lambda(lambda x: transforms.Resize((args.max_img_height, args.max_img_width))(x) if x.size[0] > x.size[1] and (x.size[0] > args.max_img_width or x.size[1] > args.max_img_height) else x),
                                            transforms.Lambda(lambda x: transforms.Resize((args.max_img_width, args.max_img_height))(x) if x.size[1] > x.size[0] and (x.size[0] > args.max_img_width or x.size[1] > args.max_img_height) else x),
                                            transforms.FiveCrop((args.img_size, args.img_size)),
                                            ])

        
    
    if args.patches_10:
        for n in range(10):
            csv_header.append('patch_' + str(n+1)) 

        ## the x in the lambda functions below represents the PIL-image
        patch_transform = transforms.Compose([transforms.Lambda(lambda x: transforms.Resize(int(round(args.img_size * 1.1)))(x) if min(x.size) < int(round(args.img_size * 1.1)) else x),
                                            transforms.Lambda(lambda x: transforms.Resize((args.max_img_height, args.max_img_width))(x) if x.size[0] > x.size[1] and (x.size[0] > args.max_img_width or x.size[1] > args.max_img_height) else x),
                                            transforms.Lambda(lambda x: transforms.Resize((args.max_img_width, args.max_img_height))(x) if x.size[1] > x.size[0] and (x.size[0] > args.max_img_width or x.size[1] > args.max_img_height) else x),
                                            transforms.TenCrop((args.img_size, args.img_size)),
                                            ])


    annotated_images = []
    if not os.path.isfile(args.csv_file):
        with open(args.csv_file, 'w', newline='') as csvfile:
            csvwriter = csv.writer(csvfile, delimiter=',',quotechar='|', quoting=csv.QUOTE_MINIMAL)
            csvwriter.writerow(csv_header)
    else:
        with open(args.csv_file, 'r', newline='') as csvfile:
            csv_reader = csv.reader(csvfile)
            next(csv_reader)
            for row in csv_reader:
                annotated_images.append(row[0])


    title_part = "press "
    for p in range(1, len(class_ids)+1):
        title_part += "{0:d} for {1:s}, ".format(p, args.classes[class_ids[p-1]])
    title_part += "and ESC to exit program"

    for j in range(len(images)):
        list = []
        full_name = images[j]
        file_name = os.path.basename(full_name)

        if file_name not in annotated_images:
            print(file_name)
            title = file_name + ' - ' + title_part
            image = Image.open(full_name).convert('RGB')
            patches = patch_transform(image)

            writeline = [file_name]

            for h in range(len(patches)):
                patch = patches[h]
                npa = np.array(patch) 
                img = npa[:, :, ::-1] # Convert RGB to BGR 
                resized = cv2.resize(img, (args.view_width, args.view_height), interpolation = cv2.INTER_AREA)

                cv2.imshow(title, resized)
                cv2.moveWindow(title, 0, 0)
                k = cv2.waitKey(0)

                for t in range(1, len(class_ids)+1):
                    if k == ord(str(t)):
                        class_annot = args.classes[class_ids[t-1]]
                        print("Patch {0:d}: class {1:d} - {2:s}".format(h+1, t, class_annot))
                        writeline.append(class_annot)

                if k == 27:
                    print('closing program')
                    breakout = True
                    break

            with open(args.csv_file, 'a', newline='') as csvfile:
                csvwriter = csv.writer(csvfile, delimiter=',',quotechar='|', quoting=csv.QUOTE_MINIMAL)
                csvwriter.writerow(writeline)
    
        cv2.destroyAllWindows() 
        if breakout:
            break

    print("Finished!")