# Training procedure of EfficientNet (image classification)

## Installation
See [INSTALL.md](../INSTALL.md)
<br/> <br/>

## Annotation procedure of the image-patches
When you want to train the algorithm on smaller (zoomed) image patches, you need to provide the correct labels of the patches. An example: when you slice the original image with apple scab into smaller patches, then some patches might not contain scab symptoms, indicating that this image patch is **healthy**. Please follow the procedure below:
<br/>
1. open a new terminal.
2. conda activate optima
3. cd optima/training/efficientnet
4. run the python file **annotate_image_patches.py**, using the appropriate arguments (in the table below).
5. the program loads all images that do not have an annotation (in the csv-file).
6. use the numbers on the keyboard to specify the class that belongs to the image-patch (the instructions are in the title of the window).
7. press ESC to quit the program. <br/> <br/>

| Argument        			| Description           												|
| --------------------------------------|-----------------------------------------------------------------------------------------------------------------------|
| --patches-5	        		| Give this command when you want to split the image into 5 smaller patches (**torchvision.transforms.FiveCrop**)	|
| --patches-10	        		| Give this command when you want to split the image into 10 smaller patches (**torchvision.transforms.TenCrop**)	|
| --classes				| A list of classes which need to be annotated										|
| --img-size				| The input image-size for the Efficientnet-model. Default is **545** for efficientnet-b5				|
| --max-img-width			| Specify the desired maximum width to transform the image to (in case the image is bigger). Default is **1600** (Adlink NEON smart-camera)					|
| --max-img-height			| Specify the desired maximum height to transform the image to (in case the image is bigger). Default is **1200** (Adlink NEON smart-camera)					|
| --view-width				| The desired width of the window for visualization purposes								|
| --view-height				| The desired height of the window for visualization purposes								|
| --img-folder     			| The file-location of the folder with the images that need to be annotated						|
| --csv-file				| The output csv-file (full-path). Include the patch-number (5 or 10) and the image resolution in the name. 		|

**Example**: 
<br/> python annotate_image_patches.py --patches-5 --classes healthy scab --img-size 545 --max-img-width 1600 --max-img-height 1200 --img-folder images --csv-file images/labels_5_image_patches_545.csv <br/> <br/> <br/> 


## Train EfficientNet
1. make or update a YAML-file with the training parameters (see table below)
2. open a new terminal.
2. conda activate optima
3. cd optima/training/efficientnet
4. run the python file **train.py**, using the YAML-file as configuration (--cfg): <br/>

**Example**: 
<br/> python train.py --cfg cfg/apple_scab.yaml <br/> <br/>

| YAML-parameter       			| Description           												|
| --------------------------------------|-----------------------------------------------------------------------------------------------------------------------|
| model	    		    		| Efficientnet-model that need to be trained. Default is **efficientnet-b5**						|
| classes				| A list of classes which need to be trained										|
| img_size				| The input image-size for the Efficientnet-model. Default is **545** for efficientnet-b5				|
| img_folder     			| The foldername that include the images in the same folder structure as the **classes**				|
| batch_size				| Optional: when **train-patches-5** or **train-patches-10** are set to **False**, specify the image batch-size for training 	|
| epochs				| The number of epochs to train the algorithm									 	|
| lr					| The learning-rate of the ADAM optimizer										|
| workers				| The number of dataloader workers. Default is **2**, but use **0** if errors occur					|
| weights_folder			| The folder the weights has to be written to										|
| weights_file				| The output filename of the weights-file (.pt)										|
| transfer_learning			| Set to **True** when you want to use transfer-learning to train the algorithm						|
| transfer_learning_file		| Optional: the filename (full-path) of the pretrained weights (.pt)							|
| transfer_learning_classes		| Optional: Specify the number of classes of the pretrained algorithm was trained on. Default is **2** or **4**		|
| train_patches_5	     		| Set to **True** when you want to split the image into 5 smaller patches and include them in the training		|
| labels_patches_5			| Optional: when **train-patches-5** is set to **True**, specify the CSV-file containing the annotations of the image patches. If it is set to **False**, then **None** would suffice. 															|
| train_patches_10    			| Set to **True** when you want to split the image into 10 smaller patches and include them in the training		|
| labels_patches_10			| Optional: when **train-patches-10** is set to **True**, specify the CSV-file containing the annotations of the image patches. If it is set to **False**, then **None** would suffice. 															|
| max_img_width			| Optional: when **train-patches-5** or **train-patches-10** are set to **True**, specify the desired maximum width to transform the image to. Default is **1600** because this corresponds to the Adlink NEON smart-camera images.										|
| max_img_height			| Optional: when **train-patches-5** or **train-patches-10** are set to **True**, specify the desired maximum height to transform the image to. Default is **1200** because this corresponds to the Adlink NEON smart-camera images.									|
| include_plant_pathology_dataset	| Set to **True** when you want to include the kaggle plant-pathology dataset in the training. Only applicable when training the is done on **apple_scab** images																	|
| plant_pathology_folder		| Optional: the foldername that include the images and annotations of the kaggle plant-pathology dataset		|
<br/>

## Export the EfficientNet weights-file (.pt) to ONNX (for image inference on the smart-camera)
1. open a new terminal.
2. conda activate optima
3. cd optima/training/efficientnet
4. run the python file **export_to_onnx.py**, using the appropriate arguments (see below): <br/> <br/>

| Argument        	| Description           												|
| ----------------------|-----------------------------------------------------------------------------------------------------------------------|
| --model	        | Efficientnet-model that need to be converted. Default is **efficientnet-b5**						|
| --weights-file     	| The file-location of the .pt file that has to be converted								|
| --batch-size		| The number of the image batch-size the algorithm has to analyse. Default is **1**. Use **6** for image patches.	|
| --num-classes	 	| The number of classes on which the Efficientnet-model was trained. Default is **2**					|
| --img-size		| The input image-size for the Efficientnet-model. Default is **545** for efficientnet-b5				|
| --onnx-file	 	| The file-location of the output ONNX file. Specify the image batch-size in the name (efficientnet_bs1 or efficientnet_bs6)																|

**Example**: 
<br/> python export_to_onnx.py --model efficientnet-b5 --weights-file weights/apple_scab/efficientnet_applescab.pt --batch-size 1 --num-classes 2 --img-size 545 --onnx-file weights/apple_scab/apple_scab_efficientnet_bs1.onnx <br/> <br/> <br/> 

## Contribution
This source-code was developed for the H2020 OPTIMA project. 
<br/> <br/>
Please contact us when you have some questions:
- Pieter Blok (pieter.blok@wur.nl)
