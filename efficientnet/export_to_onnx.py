import os
import argparse
import torch
import torch.nn as nn
import onnx
from efficientnet_pytorch import EfficientNet


if __name__ == '__main__':
  parser = argparse.ArgumentParser()
  parser.add_argument('--model', type=str, default='efficientnet-b5', help='efficientnet-model that need to be converted')
  parser.add_argument('--weights-file', type=str, default='./models/apple_scab/efficientnet_applescab.pt', help='the file-location of the .pt file that has to be converted')
  parser.add_argument('--batch-size', type=int, default=1, help='the image batch-size the model has to run on')
  parser.add_argument('--num-classes', type=int, default=2, help='the number of classes the training model has')
  parser.add_argument('--img-size', type=int, default=545, help='the input image-size for the efficientnet-model')
  parser.add_argument('--onnx-file', type=str, default='./models/apple_scab/apple_scab_efficientnetb5.onnx', help='the file-location of the output ONNX file')

  args = parser.parse_args()

  model = EfficientNet.from_pretrained(args.model)
  dummy_input = torch.randn(args.batch_size, 3, args.img_size, args.img_size)

  num_ftrs = model._fc.in_features
  model._fc = nn.Sequential(nn.Linear(num_ftrs,1000,bias=True),
                            nn.ReLU(),
                            nn.Dropout(p=0.5),
                            nn.Linear(1000, args.num_classes, bias = True))

  model.load_state_dict(torch.load(args.weights_file))
  model.eval()
  model.set_swish(memory_efficient=False)
  torch.onnx.export(model, dummy_input, args.onnx_file, verbose=False)