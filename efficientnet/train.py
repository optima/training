# @Author: Pieter Blok
# @Date:   2021-03-15 09:18:34
# @Last Modified by:   Pieter Blok
# @Last Modified time: 2021-04-01 16:25:28
import sys
import os
import argparse
import time
import yaml

import torch
import torch.utils.data as Data
import torch.nn as nn
from torch.autograd import Variable
from torchvision import transforms, models
import numpy as np
np.set_printoptions(formatter={'float_kind':'{:f}'.format})

import pandas as pd
from PIL import Image
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.metrics import accuracy_score, confusion_matrix
from sklearn.utils import shuffle
from scipy.special import softmax
from efficientnet_pytorch import EfficientNet
from utils.cf_matrix import make_confusion_matrix

import cv2
from transformers import get_cosine_schedule_with_warmup
from transformers import AdamW
from tqdm import tqdm
import gc
from sklearn.model_selection import train_test_split

import warnings
warnings.filterwarnings("ignore")

supported_cv2_formats = (".bmp", ".dib", ".jpeg", ".jpg", ".jpe", ".jp2", ".png", ".pbm", ".pgm", ".ppm", ".sr", ".ras", ".tiff", ".tif")

def check_direxcist(dir):
    if dir is not None:
        if not os.path.exists(dir):
            os.makedirs(dir)

## dataset class with some data augmentations
class CustomDataset(Data.Dataset):
    def __init__(self, image_paths, image_size, labels = None, train = True, patches_5 = False, patches_10 = False, max_img_width=1600, max_img_height=1200):
        self.paths = image_paths
        self.labels = labels
        self.train = train
        self.patches_5 = patches_5
        self.patches_10 = patches_10

        ## train transformations (including the image patch transformations)
        self.train_transform = transforms.Compose([transforms.Resize(int(round(image_size * 1.1))),
                                                    transforms.CenterCrop(image_size),
                                                    transforms.ColorJitter(hue=.1, saturation=.1),
                                                    transforms.RandomHorizontalFlip(0.5),
                                                    transforms.RandomRotation(90, resample=Image.BILINEAR),
                                                    transforms.ToTensor(),
                                                    transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225]),])

        ## the x in the lambda functions below represents the PIL-image
        self.train_patch_5_transform = transforms.Compose([transforms.Lambda(lambda x: transforms.Resize(int(round(image_size * 1.1)))(x) if min(x.size) < int(round(image_size * 1.1)) else x),
                                                    transforms.Lambda(lambda x: transforms.Resize((max_img_height, max_img_width))(x) if x.size[0] > x.size[1] and (x.size[0] > max_img_width or x.size[1] > max_img_height) else x),
                                                    transforms.Lambda(lambda x: transforms.Resize((max_img_width, max_img_height))(x) if x.size[1] > x.size[0] and (x.size[0] > max_img_width or x.size[1] > max_img_height) else x),
                                                    transforms.FiveCrop((image_size, image_size)),
                                                    transforms.Lambda(lambda crops: [transforms.ColorJitter(hue=.1, saturation=.1)(crop) for crop in crops]),
                                                    transforms.Lambda(lambda crops: [transforms.RandomHorizontalFlip(0.5)(crop) for crop in crops]),
                                                    transforms.Lambda(lambda crops: [transforms.RandomRotation(90, resample=Image.BILINEAR)(crop) for crop in crops]),
                                                    transforms.Lambda(lambda crops: torch.stack([transforms.ToTensor()(crop) for crop in crops])),
                                                    transforms.Lambda(lambda tensors:
                                                    torch.stack([transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225])(t) for t in tensors]))])

        ## the x in the lambda functions below represents the PIL-image
        self.train_patch_10_transform = transforms.Compose([transforms.Lambda(lambda x: transforms.Resize(int(round(image_size * 1.1)))(x) if min(x.size) < int(round(image_size * 1.1)) else x),
                                                    transforms.Lambda(lambda x: transforms.Resize((max_img_height, max_img_width))(x) if x.size[0] > x.size[1] and (x.size[0] > max_img_width or x.size[1] > max_img_height) else x),
                                                    transforms.Lambda(lambda x: transforms.Resize((max_img_width, max_img_height))(x) if x.size[1] > x.size[0] and (x.size[0] > max_img_width or x.size[1] > max_img_height) else x),
                                                    transforms.TenCrop((image_size, image_size)),
                                                    transforms.Lambda(lambda crops: [transforms.ColorJitter(hue=.1, saturation=.1)(crop) for crop in crops]),
                                                    transforms.Lambda(lambda crops: [transforms.RandomHorizontalFlip(0.5)(crop) for crop in crops]),
                                                    transforms.Lambda(lambda crops: [transforms.RandomRotation(90, resample=Image.BILINEAR)(crop) for crop in crops]),
                                                    transforms.Lambda(lambda crops: torch.stack([transforms.ToTensor()(crop) for crop in crops])),
                                                    transforms.Lambda(lambda tensors:
                                                    torch.stack([transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225])(t) for t in tensors]))])


        ## test transformations (including the image patch transformations)
        self.test_transform = transforms.Compose([transforms.Resize(int(round(image_size * 1.1))),
                                                    transforms.CenterCrop(image_size),
                                                    transforms.ToTensor(),
                                                    transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225]),])

        ## the x in the lambda functions below represents the PIL-image
        self.test_patch_5_transform = transforms.Compose([transforms.Lambda(lambda x: transforms.Resize(int(round(image_size * 1.1)))(x) if min(x.size) < int(round(image_size * 1.1)) else x),
                                                    transforms.Lambda(lambda x: transforms.Resize((max_img_height, max_img_width))(x) if x.size[0] > x.size[1] and (x.size[0] > max_img_width or x.size[1] > max_img_height) else x),
                                                    transforms.Lambda(lambda x: transforms.Resize((max_img_width, max_img_height))(x) if x.size[1] > x.size[0] and (x.size[0] > max_img_width or x.size[1] > max_img_height) else x),
                                                    transforms.FiveCrop((image_size, image_size)), # this is a list of PIL Images
                                                    transforms.Lambda(lambda crops: torch.stack([transforms.ToTensor()(crop) for crop in crops])),
                                                    transforms.Lambda(lambda tensors:
                                                    torch.stack([transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225])(t) for t in tensors]))])

        ## the x in the lambda functions below represents the PIL-image
        self.test_patch_10_transform = transforms.Compose([transforms.Lambda(lambda x: transforms.Resize(int(round(image_size * 1.1)))(x) if min(x.size) < int(round(image_size * 1.1)) else x),
                                                    transforms.Lambda(lambda x: transforms.Resize((max_img_height, max_img_width))(x) if x.size[0] > x.size[1] and (x.size[0] > max_img_width or x.size[1] > max_img_height) else x),
                                                    transforms.Lambda(lambda x: transforms.Resize((max_img_width, max_img_height))(x) if x.size[1] > x.size[0] and (x.size[0] > max_img_width or x.size[1] > max_img_height) else x),
                                                    transforms.TenCrop((image_size, image_size)), # this is a list of PIL Images
                                                    transforms.Lambda(lambda crops: torch.stack([transforms.ToTensor()(crop) for crop in crops])),
                                                    transforms.Lambda(lambda tensors:
                                                    torch.stack([transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225])(t) for t in tensors]))])

    def __len__(self):
        return self.paths.shape[0]
    
    def __getitem__(self, i):
        image = Image.open(self.paths[i]).convert('RGB') #load the img file!

        if self.train:
            tensor = self.train_transform(image).float()
            label = torch.tensor(np.argmax(self.labels.loc[i,:].values))

            if self.patches_5:
                tensor = tensor.unsqueeze(0)
                patches = self.train_patch_5_transform(image).float()
                tensor = torch.cat((tensor, patches), dim=0)
                all_labels = self.labels.loc[i,:].values
                splits = np.array_split(all_labels, 6)

                labels = []
                for k in range(len(splits)):
                    labels.append(np.argmax(splits[k]))
                labels = np.asarray(labels).astype(np.int64)
                label = torch.tensor(labels)

            if self.patches_10:
                tensor = tensor.unsqueeze(0)
                patches = self.train_patch_10_transform(image).float()
                tensor = torch.cat((tensor, patches), dim=0)
                all_labels = self.labels.loc[i,:].values
                splits = np.array_split(all_labels, 11)

                labels = []
                for k in range(len(splits)):
                    labels.append(np.argmax(splits[k]))
                labels = np.asarray(labels).astype(np.int64)
                label = torch.tensor(labels)

        else:
            tensor = self.test_transform(image).float()
            label = torch.tensor(np.argmax(self.labels.loc[i,:].values))

            if self.patches_5:
                tensor = tensor.unsqueeze(0)
                patches = self.test_patch_5_transform(image).float()
                tensor = torch.cat((tensor, patches), dim=0)
                all_labels = self.labels.loc[i,:].values
                splits = np.array_split(all_labels, 6)

                labels = []
                for k in range(len(splits)):
                    labels.append(np.argmax(splits[k]))
                labels = np.asarray(labels).astype(np.int64)
                label = torch.tensor(labels)

            if self.patches_10:
                tensor = tensor.unsqueeze(0)
                patches = self.test_patch_10_transform(image).float()
                tensor = torch.cat((tensor, patches), dim=0)
                all_labels = self.labels.loc[i,:].values
                splits = np.array_split(all_labels, 11)

                labels = []
                for k in range(len(splits)):
                    labels.append(np.argmax(splits[k]))
                labels = np.asarray(labels).astype(np.int64)
                label = torch.tensor(labels)

        return tensor, label


def get_image_path(imgfolder, filename):
    writename = filename + '.jpg'
    return (os.path.join(imgfolder, writename))


def list_images(imagedir):
    if os.path.isdir(imagedir):
        all_files = os.listdir(imagedir)
        images = [os.path.join(imagedir, x) for x in all_files if x.lower().endswith(supported_cv2_formats)]
        images.sort()
    return images


def train_fn(net, loader):
    running_loss = 0
    preds_for_acc = []
    labels_for_acc = []
    
    pbar = tqdm(total = len(loader), desc='Training')
    
    for _, (images, labels) in enumerate(loader):
        
        images, labels = images.to(device), labels.to(device)
        net.train()
        optimizer.zero_grad()
        predictions = net(images)
        loss = loss_fn(predictions, labels)
        loss.backward()
        optimizer.step()
        scheduler.step()
        
        running_loss += loss.item()*labels.shape[0]
        labels_for_acc = np.concatenate((labels_for_acc, labels.cpu().numpy()), 0)
        preds_for_acc = np.concatenate((preds_for_acc, np.argmax(predictions.cpu().detach().numpy(), 1)), 0)
        
        pbar.update()
        
    accuracy = accuracy_score(labels_for_acc, preds_for_acc)
    
    pbar.close()
    return running_loss/train_size, accuracy


def valid_fn(net, loader):
    running_loss = 0
    preds_for_acc = []
    labels_for_acc = []
    
    pbar = tqdm(total = len(loader), desc='Validation')
    
    with torch.no_grad():       #torch.no_grad() prevents Autograd engine from storing intermediate values, saving memory
        for _, (images, labels) in enumerate(loader):
            
            images, labels = images.to(device), labels.to(device)
            net.eval()
            predictions = net(images)
            loss = loss_fn(predictions, labels)
            
            running_loss += loss.item()*labels.shape[0]
            labels_for_acc = np.concatenate((labels_for_acc, labels.cpu().numpy()), 0)
            preds_for_acc = np.concatenate((preds_for_acc, np.argmax(predictions.cpu().detach().numpy(), 1)), 0)
            
            pbar.update()
            
        accuracy = accuracy_score(labels_for_acc, preds_for_acc)
        conf_mat = confusion_matrix(labels_for_acc, preds_for_acc)
    
    pbar.close()
    return running_loss/valid_size, accuracy, conf_mat


def test_fn(net, loader):
    preds_for_acc = []
    labels_for_acc = []
    
    pbar = tqdm(total = len(loader), desc='Testing')
    
    with torch.no_grad():       #torch.no_grad() prevents Autograd engine from storing intermediate values, saving memory
        for _, (images, labels) in enumerate(loader):
            
            images, labels = images.to(device), labels.to(device)
            net.eval()
            predictions = net(images)

            labels_for_acc = np.concatenate((labels_for_acc, labels.cpu().numpy()), 0)
            preds_for_acc = np.concatenate((preds_for_acc, np.argmax(predictions.cpu().detach().numpy(), 1)), 0)
            
            pbar.update()
            
        accuracy = accuracy_score(labels_for_acc, preds_for_acc)
        conf_mat = confusion_matrix(labels_for_acc, preds_for_acc)
    
    pbar.close()
    return accuracy, conf_mat


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--cfg', type=str, default='apple_scab.yaml', help='yaml with the training parameters')
    args = parser.parse_args()

    try:
        with open(args.cfg, 'rb') as file:
            cfg = yaml.load(file, Loader=yaml.FullLoader)
    except FileNotFoundError:
        sys.exit(f"Could not find settings file ({args.cfg}), closing application")

    print(cfg)

    if cfg['train_patches_5']:
        repeat = 6
        labels_patches = pd.read_csv(cfg['labels_patches_5'])
    if cfg['train_patches_10']:
        repeat = 11
        labels_patches = pd.read_csv(cfg['labels_patches_10'])

    check_direxcist(cfg['weights_folder'])

    ## prepare dataset and set the labels with one-hot encoding
    header = []
    header.append('image_id')

    if cfg['train_patches_5'] or cfg['train_patches_10']:
        for _ in range(repeat):
            header.extend(cfg['classes'])
    else: 
        header.extend(cfg['classes'])

    header.append('image_path')

    npc = np.asarray(cfg['classes']).astype(np.str)
    c = pd.Series(cfg['classes'])
    one_hot = pd.get_dummies(c)
    one_hot = one_hot.to_numpy()
    data = []

    for i in range(len(cfg['classes'])):
        classname = cfg['classes'][i]
        images = list_images(os.path.join(cfg['img_folder'], classname))

        for j in range(len(images)):
            list = []
            full_name = images[j]
            file_name = os.path.basename(full_name)
            list.append(file_name)

            ## load the labels of the image-patches
            if cfg['train_patches_5'] or cfg['train_patches_10']:
                list.extend(one_hot[i].tolist())
                idx = labels_patches[labels_patches['image_id']==file_name].index.values[0]
                lp = labels_patches.loc[idx].to_numpy()[1:]
                for c in range(len(lp)):
                    class_id = np.where(npc == lp[c])[0][0]
                    list.extend(one_hot[class_id].tolist())
            else:
                list.extend(one_hot[i].tolist())

            list.append(full_name)
            data.append(list)

    df = pd.DataFrame(data, columns=header)
    df = shuffle(df)
    train, test = train_test_split(df, test_size = cfg['val_test_ratio'])


    ## final preparation of the datasets
    train = shuffle(train)
    train_labels = train.iloc[:, 1:train.shape[-1]-1]  #train_labels = train.loc[:, cfg['classes'][0]:cfg['classes'][-1]]
    train_paths = train.image_path

    try:
        train_paths, valid_paths, train_labels, valid_labels = train_test_split(train_paths, train_labels, test_size = cfg['val_test_ratio'], random_state=23, stratify = train_labels)
    except:
        train_paths, valid_paths, train_labels, valid_labels = train_test_split(train_paths, train_labels, test_size = cfg['val_test_ratio'])
    
    test = shuffle(test)
    test_labels = test.iloc[:, 1:test.shape[-1]-1] #test_labels = test.loc[:, cfg['classes'][0]:cfg['classes'][-1]]
    test_paths = test.image_path

    train_paths.reset_index(drop=True, inplace=True)
    train_labels.reset_index(drop=True, inplace=True)
    valid_paths.reset_index(drop=True, inplace=True)
    valid_labels.reset_index(drop=True, inplace=True)
    test_paths.reset_index(drop=True, inplace=True)
    test_labels.reset_index(drop=True, inplace=True)

    train_size = train_labels.shape[0]
    valid_size = valid_labels.shape[0]
    test_size = test_labels.shape[0]


    ## create the dataloaders
    train_dataset = CustomDataset(train_paths, cfg['img_size'], train_labels, patches_5 = cfg['train_patches_5'], patches_10 = cfg['train_patches_10'], max_img_width=cfg['max_img_width'], max_img_height=cfg['max_img_height'])
    valid_dataset = CustomDataset(valid_paths, cfg['img_size'], valid_labels, train = False, patches_5 = cfg['train_patches_5'], patches_10 = cfg['train_patches_10'], max_img_width=cfg['max_img_width'], max_img_height=cfg['max_img_height'])
    test_dataset = CustomDataset(test_paths, cfg['img_size'], test_labels, train = False, patches_5 = cfg['train_patches_5'], patches_10 = cfg['train_patches_10'], max_img_width=cfg['max_img_width'], max_img_height=cfg['max_img_height'])
    
    if cfg['train_patches_5'] or cfg['train_patches_10']:
        trainloader = Data.DataLoader(train_dataset, shuffle=True, batch_size = None, batch_sampler=None, num_workers = cfg['workers'])
        validloader = Data.DataLoader(valid_dataset, shuffle=False, batch_size = None, batch_sampler=None, num_workers = cfg['workers'])
        testloader = Data.DataLoader(test_dataset, shuffle=False, batch_size = None, batch_sampler=None, num_workers = cfg['workers'])
    else:
        trainloader = Data.DataLoader(train_dataset, shuffle=True, batch_size = cfg['batch_size'], num_workers = cfg['workers'])
        validloader = Data.DataLoader(valid_dataset, shuffle=False, batch_size = cfg['batch_size'], num_workers = cfg['workers'])
        testloader = Data.DataLoader(test_dataset, shuffle=False, batch_size = cfg['batch_size'], num_workers = cfg['workers'])


    ## initialize the training parameters (including transfer-learning)
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    model = EfficientNet.from_pretrained(cfg['model'])
    num_ftrs = model._fc.in_features

    ## load the pretrained model that was trained on the plant_pathology dataset
    if cfg['transfer_learning']:
        model._fc = nn.Sequential(nn.Linear(num_ftrs,1000,bias=True),
                                nn.ReLU(),
                                nn.Dropout(p=0.5),
                                nn.Linear(1000, cfg['transfer_learning_classes'], bias = True))
        model.load_state_dict(torch.load(cfg['transfer_learning_file']))


    ## now change the output-layers of the pretrained model to our use-case
    model._fc = nn.Sequential(nn.Linear(num_ftrs,1000,bias=True),
                            nn.ReLU(),
                            nn.Dropout(p=0.5),
                            nn.Linear(1000, len(cfg['classes']), bias = True))
    model.to(device)


    ## initialize the optimizers and loss function for training
    optimizer = AdamW(model.parameters(), lr = cfg['lr'], weight_decay = 1e-3)
    num_train_steps = int(len(train_dataset) / cfg['batch_size'] * cfg['epochs'])
    scheduler = get_cosine_schedule_with_warmup(optimizer, num_warmup_steps=len(train_dataset)/cfg['batch_size']*5, num_training_steps=num_train_steps)
    loss_fn = torch.nn.CrossEntropyLoss()


    ## initialize the losses
    train_loss = []
    valid_loss = []
    train_acc = []
    val_acc = []
    best_loss = 10000.0


    ## main training loop
    for epoch in range(cfg['epochs']):
        tl, ta = train_fn(model, loader = trainloader)
        vl, va, conf_mat = valid_fn(model, loader = validloader)
        train_loss.append(tl)
        valid_loss.append(vl)
        train_acc.append(ta)
        val_acc.append(va)
        
        printstr = 'Epoch: {:d}/{:d}, Train loss: {:.3f}, Val loss: {:.3f}, Train acc: {:.3f}, Val acc: {:.3f}'.format(epoch+1, cfg['epochs'], tl, vl, ta, va)
        tqdm.write(printstr)

        if vl < best_loss:
            best_loss = vl
            print('Saving model..')
            torch.save(model.state_dict(), os.path.join(cfg['weights_folder'], cfg['weights_file']))


    ## Plot the loss-curves
    plt.figure()
    sns.lineplot(np.arange(len(train_loss)).tolist(), train_loss)
    sns.lineplot(np.arange(len(valid_loss)).tolist(), valid_loss)
    plt.xlabel('Epoch')
    plt.ylabel('Loss')
    plt.legend(['Train','Val'])
    plt.savefig(os.path.join(cfg['weights_folder'], 'loss_curve.png'), bbox_inches='tight')
    plt.close()


    ## Plot the accuracy-curves
    plt.figure()
    sns.lineplot(np.arange(len(train_acc)).tolist(), train_acc)
    sns.lineplot(np.arange(len(val_acc)).tolist(), val_acc)
    plt.xlabel('Epoch')
    plt.ylabel('Accuracy')
    plt.legend(['Train','Val'])
    plt.savefig(os.path.join(cfg['weights_folder'], 'accuracy_curve.png'), bbox_inches='tight')
    plt.close()


    ## Plot the confusion matrix on the best model
    del model
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    model = EfficientNet.from_pretrained(cfg['model'])
    num_ftrs = model._fc.in_features
    model._fc = nn.Sequential(nn.Linear(num_ftrs,1000,bias=True),
                            nn.ReLU(),
                            nn.Dropout(p=0.5),
                            nn.Linear(1000, len(cfg['classes']), bias = True))

    model.load_state_dict(torch.load(os.path.join(cfg['weights_folder'], cfg['weights_file'])))
    model.to(device)

    ta, conf_mat = test_fn(model, loader = testloader)
    plt.figure()
    sns.set_context('talk')
    labels = ['TN','FP','FN','TP']
    categories = []
    categories.extend(cfg['classes'])
    make_confusion_matrix(conf_mat, group_names=labels, categories=categories, cbar=False, title='confusion matrix')
    plt.savefig(os.path.join(cfg['weights_folder'], 'confusion_matrix.png'), bbox_inches='tight')
    plt.close()

    ## Clear GPU memory
    del model
    del optimizer
    del scheduler
    torch.cuda.empty_cache()