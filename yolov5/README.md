# Training procedure of YOLOv5 (object detection)

## Installation of the software
See [INSTALL.md](../INSTALL.md)
<br/> <br/>

## Annotation procedure
We recommend to annotate the diseased-spots with the labelImg program (https://github.com/tzutalin/labelImg):
<br/><br/>
**IMPORTANT NOTE**
<br/> labelImg sometimes wrongly rotates the image, causing rotated and erroneous bounding-boxes (https://github.com/tzutalin/labelImg/issues/159). This has to do with the EXIF file. To prevent this error, you first need to convert all images in the annotation folder with a exiftran tool. After the conversion you can use labelImg to annotate your images. The exiftran procedure is as follows (on Ubuntu):
- sudo apt-get install -y exiftran
- navigate to the image-folder (cd ...)
- exiftran -ai *.JPG (or another file-extension of your images)
<br/> <br/>

**labelImg annotation procedure:**
1. Open the **labelImg** program
2. First, change the output format to **YOLO** (click on the **Pascal/VOC** button so that it changes to YOLO)
3. Click the button **Open Dir**, and navigate to a local folder with the images of your use-case (apples, carrots, grapes)
4. If there is a diseased spot in the image, click the button **Create RectBox**. Then, draw the rectangle over the diseased spot (see a few examples in the image below).
5. When finished click **Ok**. Specify the correct class label (for example apple_scab, downy_mildew or alternaria). 
6. In case there are more diseased spots, please repeat the process by drawing another bounding box (with **Create RectBox**). 
7. In case you want to modify the bounding box after clicking Ok: go with the mouse to the corner points of the bounding box and move them (the cornerpoint turns red). 
8. When all objects are annotated in the image click the button **Save**. Then click again **Save** (don’t change the name of the output-file!). 
9. Go the next image in the folder by clicking the **Next Image** button.
10. In case of there is no diseased spot in the image, just click the button **Next Image**. 
11. The annotations are stored in **.txt** files.

<br/> <br/> ![labelImg annotation](./data/annotation_example.jpg?raw=true)
<br/> *All diseased leaves are annotated by a bounding box (red rectangle).*
<br/> <br/>

## Prepare the datasets
After the image annotation, randomly split the images in a training, a validation and a test set. Put the images in the following folder-structure:
- data/images/train
- data/images/val
- data/images/test <br/>

Put the annotation-files in different folders (be sure that the .txt files correspond to the images in the **images** folders):
- data/labels/train
- data/labels/val
- data/labels/test <br/>

Change the image-folders in the YAML-file (data/apple_scab.yaml), for example:
- train: data/images/train/ 
- val: data/images/val/
<br/> <br/>

## Train yolov5
1. open a new terminal.
2. conda activate optima
3. cd optima/training/yolov5
4. run the python file **train.py**, using the appropriate arguments (see below): <br/> <br/>

| Argument        	| Description           															|
| ----------------------|-----------------------------------------------------------------------------------------------------------------------------------------------|
| --img		        | The input image-size for the yolov5-model. Default is **640** 										|
| --batch	     	| The batch-size for the training. Default is **24**												|
| --epochs	 	| The number of training epochs. Default is **300**	  											|
| --data		| YAML-file with the specifications of your dataset, for example apple_scab.yaml								|
| --weights	 	| Pretrained weights that are used for transfer-learning. Choose the right model (yolov5s, yolov5m, yolov5l, yolov5x)				|
| --name		| The name that is assigned to the training. Choose a name that can be remembered easily. 							|

<br/>
Example syntax: 
<br/> python train.py --img 640 --batch 24 --epochs 300 --data apple_scab.yaml --weights weights/yolov5s.pt --name apple_scab_20200921_yolov5s <br/> <br/> 

The weights are stored in the folder **runs/train** under the name you specified with the argument --name. 
<br/> <br/> 


## Detection metrics (confusion matrix)
1. open a new terminal.
2. conda activate optima
3. cd optima/training/yolov5
4. run the python file **detect.py**: 
<br/> python detect.py --source ./data/apple_scab_20210610/images/test --weights ./runs/train/apple_scab_20210610_yolov5s/weights/best.pt --img-size 640 --conf-thres 0.3 --iou-thres 0.3 --name apple_scab_20210610_yolov5s --save-txt <br/>
5. run the python file **detection_metrics.py**: 
<br/> python detection_metrics.py --img_dir ./data/apple_scab_20210610/images/test --annot_dir ./data/apple_scab_20210610/labels/test --result_dir ./runs/detect/apple_scab_20210610_yolov5s/labels --write_dir ./runs/detect/apple_scab_20210610_yolov5s/detection_metrics --iou-thres 0.5 --name apple_scab_20210610_yolov5s --save-txt <br/> <br/>


## Export the yolov5 weights-file (.pt) to ONNX (for image inference on the smart-camera)
1. open a new terminal.
2. conda activate optima
3. cd optima/training/yolov5
4. run the python file **models/export.py**, using the appropriate arguments (see below): <br/> <br/>

| Argument        	| Description           												|
| ----------------------|-----------------------------------------------------------------------------------------------------------------------|
| --weights	        | The file-location of the .pt file that has to be converted								|
| --img			| The input image-size for the yolov5-model. Default is **640** 							|
| --batch	 	| The batch-size for the image inference. Default is **1**								|

Example syntax: 
<br/> python models/export.py --weights weights/apple_scab/apple_scab_20200921_yolov5s.pt --img 640 --batch 1 <br/> <br/> <br/> 


## Contribution
This source-code was developed for the H2020 OPTIMA project. 
<br/> <br/>
Please contact us when you have some questions:
- Pieter Blok (pieter.blok@wur.nl)
