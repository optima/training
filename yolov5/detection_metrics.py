# -*- coding: utf-8 -*-
# @Author: Pieter Blok
# @Date:   1970-01-01 09:00:00
# @Last Modified by:   Pieter Blok
# @Last Modified time: 2021-12-24 11:32:19

import os
import argparse
import numpy as np
import cv2
from tqdm import tqdm

supported_cv2_formats = (".bmp", ".dib", ".jpeg", ".jpg", ".jpe", ".jp2", ".png", ".pbm", ".pgm", ".ppm", ".sr", ".ras", ".tiff", ".tif")

def list_files(rootdir):
    images = []
    annotations = []

    if os.path.isdir(rootdir):
        for root, dirs, files in list(os.walk(rootdir)):
            for name in files:
                if name.lower().endswith(supported_cv2_formats):
                    images.append(name)
    
        images.sort()

    return images


def check_direxcist(dir):
    if dir is not None:
        if not os.path.exists(dir):
            os.makedirs(dir)  # make new folder


def process_label_file(dirname, filename):
    bb_num = 0
    tlcs = []
    brcs = []
    if os.path.isfile(os.path.join(dirname, filename)):
        with open(os.path.join(dirname, filename), 'r') as fp:
            for line in fp:
                if len(line.split(" ")) == 5:
                    bb_num += 1
                    x = float(line.split(" ")[1])
                    y = float(line.split(" ")[2])
                    w = float(line.split(" ")[3])
                    hh = line.split(" ")[4]
                    h = float(hh.split("\n")[0])
                    
                    xx = x * width
                    yy = y * height
                    ww = w * width
                    hh = h * height
                    
                    tlcs.append((int(xx - (0.5*ww)), int(yy - (0.5*hh))))
                    brcs.append((int(xx + (0.5*ww)), int(yy + (0.5*hh))))
    return tlcs, brcs, bb_num


## function from: https://stackoverflow.com/questions/25349178/calculating-percentage-of-bounding-box-overlap-for-image-detector-evaluation
def get_iou(bb1, bb2):
    """
    Calculate the Intersection over Union (IoU) of two bounding boxes.

    Parameters
    ----------
    bb1 : dict
        Keys: {'x1', 'x2', 'y1', 'y2'}
        The (x1, y1) position is at the top left corner,
        the (x2, y2) position is at the bottom right corner
    bb2 : dict
        Keys: {'x1', 'x2', 'y1', 'y2'}
        The (x, y) position is at the top left corner,
        the (x2, y2) position is at the bottom right corner

    Returns
    -------
    float
        in [0, 1]
    """
    assert bb1['x1'] < bb1['x2']
    assert bb1['y1'] < bb1['y2']
    assert bb2['x1'] < bb2['x2']
    assert bb2['y1'] < bb2['y2']

    # determine the coordinates of the intersection rectangle
    x_left = max(bb1['x1'], bb2['x1'])
    y_top = max(bb1['y1'], bb2['y1'])
    x_right = min(bb1['x2'], bb2['x2'])
    y_bottom = min(bb1['y2'], bb2['y2'])

    if x_right < x_left or y_bottom < y_top:
        return 0.0

    # The intersection of two axis-aligned bounding boxes is always an
    # axis-aligned bounding box
    intersection_area = (x_right - x_left) * (y_bottom - y_top)

    # compute the area of both AABBs
    bb1_area = (bb1['x2'] - bb1['x1']) * (bb1['y2'] - bb1['y1'])
    bb2_area = (bb2['x2'] - bb2['x1']) * (bb2['y2'] - bb2['y1'])

    # compute the intersection over union by taking the intersection
    # area and dividing it by the sum of prediction + ground-truth
    # areas - the interesection area
    iou = intersection_area / float(bb1_area + bb2_area - intersection_area)
    assert iou >= 0.0
    assert iou <= 1.0
    return iou


def iou_matrix(gt_tlcs, gt_brcs, gt_num, det_tlcs, det_brcs, det_num):
    IoU_matrix = np.zeros((gt_num, det_num)).astype(dtype=np.float32)

    for i in range (det_num):
        det_x1 = det_tlcs[i][0]
        det_y1 = det_tlcs[i][1]
        det_x2 = det_brcs[i][0]
        det_y2 = det_brcs[i][1]

        det_bb = {"x1": det_x1, "x2": det_x2, "y1": det_y1, "y2": det_y2}

        for k in range (gt_num):
            gt_x1 = gt_tlcs[k][0]
            gt_y1 = gt_tlcs[k][1]
            gt_x2 = gt_brcs[k][0]
            gt_y2 = gt_brcs[k][1]

            gt_bb = {"x1": gt_x1, "x2": gt_x2, "y1": gt_y1, "y2": gt_y2}

            IoU = get_iou(det_bb, gt_bb)
            IoU_matrix[k,i] = IoU
    return IoU_matrix


def assign_tps(IoU_matrix, iou_thres):
    a = np.empty([IoU_matrix.shape[0], IoU_matrix.shape[1]], dtype='U2')
    tp_row, tp_col = np.where(IoU_matrix>=iou_thres)

    for tr in range(len(tp_row)):
        row_loc = tp_row[tr]
        col_loc = tp_col[tr]
        a[row_loc, col_loc] = "tp"

    return a


def draw_img(img, tlc, brc, metric, color):
    x1 = tlc[0]
    y1 = tlc[1]

    img = cv2.rectangle(img, tlc, brc, color, 10)
    text_str = metric

    font_face = cv2.FONT_HERSHEY_DUPLEX

    text_w, text_h = cv2.getTextSize(text_str, font_face, 2, 2)[0]
    text_pt = (x1, y1 - 3)
    text_color = [0, 0, 0]

    img = cv2.rectangle(img, (x1, y1), (x1 + text_w, y1 - text_h - 4), color, -1)
    img = cv2.putText(img, text_str, text_pt, font_face, 2, text_color, 2, cv2.LINE_AA)
    return img


if __name__ == "__main__":
    ## Initialize the args_parser and some variables
    parser = argparse.ArgumentParser()
    parser.add_argument('--img_dir', type=str, default='./images/test', help='the folder with images that need to be tested')
    parser.add_argument('--annot_dir', type=str, default='./labels/test', help='the folder with annotations')
    parser.add_argument('--result_dir', type=str, default='./runs/detect', help='the folder with resulting txt files')
    parser.add_argument('--write_dir', type=str, default='./runs/detect', help='the folder to write the resulting images')
    parser.add_argument('--iou_thres', type=float, default=0.5, help='intersection over union threshold')

    ## Load the args_parser and initialize some variables
    opt = parser.parse_args()
    print(opt)
    print()

    images = list_files(opt.img_dir)

    check_direxcist(opt.write_dir)
    txtfile = open(os.path.join(opt.write_dir, "detection_metrics.txt"),"w")
    txtfile.write("ImageName,True Positive count,False Positive count,False Negative count\r\n")
    txtfile.close() 

    breakout = False
    tp_sum = 0
    fp_sum = 0
    fn_sum = 0
    gt_sum = 0
    det_sum = 0

    for j in tqdm(range(len(images))):
        tp = 0
        fn = 0
        fp = 0 
        
        imgname = images[j]
        img = cv2.imread(os.path.join(opt.img_dir, imgname))
        img_vis = img.copy()
        
        height, width = img.shape[:2]
        basename, file_extension = os.path.splitext(imgname)
        txtfile = basename + '.txt'

        gt_tlcs, gt_brcs, gt_num = process_label_file(opt.annot_dir, txtfile)
        det_tlcs, det_brcs, det_num = process_label_file(opt.result_dir, txtfile)
        
        if det_num != 0 and gt_num != 0:
            IoU_matrix_present = True
            IoU_matrix = iou_matrix(gt_tlcs, gt_brcs, gt_num, det_tlcs, det_brcs, det_num)
        else:
            IoU_matrix_present = False
            if det_num != 0 and gt_num == 0:
                fp += det_num
                for n in range(det_num):
                    img_vis = draw_img(img_vis, det_tlcs[n], det_brcs[n], "FP", (255, 0, 0))
                    
            if det_num == 0 and gt_num != 0:
                fn += gt_num
                for n in range(gt_num):
                    img_vis = draw_img(img_vis, gt_tlcs[n], gt_brcs[n], "FN", (0, 0, 255))
            
        if IoU_matrix_present:
            a = assign_tps(IoU_matrix, opt.iou_thres)
            tp = np.count_nonzero(a == 'tp')

            for row in range(a.shape[0]):
                if "tp" not in a[row]:
                    fn += 1
                    img_vis = draw_img(img_vis, gt_tlcs[row], gt_brcs[row], "FN", (0, 0, 255))

            for col in range(a.shape[1]):
                if "tp" in a[:,col]:
                    img_vis = draw_img(img_vis, det_tlcs[col], det_brcs[col], "TP", (0, 255, 0))

                if "tp" not in a[:,col]:
                    fp += 1
                    img_vis = draw_img(img_vis, det_tlcs[col], det_brcs[col], "FP", (255, 0, 0))

        gt_sum += gt_num
        det_sum += det_num
        
        tp_sum += tp
        fp_sum += fp
        fn_sum += fn

        wn = basename+'.jpg'
        cv2.imwrite(os.path.join(opt.write_dir, wn), img_vis)

        txtfile = open(os.path.join(opt.write_dir, "detection_metrics.txt"),"a")
        txtfile.write(imgname + "," + str(tp_sum) + "," + str(fp_sum) + "," + str(fn_sum) + "\r\n")
        txtfile.close() 

    try:
        recall = tp_sum / (tp_sum + fn_sum)
        precision = tp_sum / (tp_sum + fp_sum)
        f1 = (2*recall*precision) / (recall+precision)
    except:
        recall = 0
        precision = 0
        f1 = 0 

    print("The total number of annotated boxes: {:d} (TP + FN: {:d})".format(gt_sum, tp_sum + fn_sum))
    print("The total number of detected boxes: {:d} (TP + FP: {:d})".format(det_sum, tp_sum + fp_sum))
    print("")
    
    print("The total number of true positives is: {:d}".format(tp_sum))
    print("The total number of false negatives is: {:d}".format(fn_sum))
    print("The total number of false postives is: {:d}".format(fp_sum))
    print("")

    print("The recall is: {:.1f}%".format(recall * 100))
    print("The precision is: {:.1f}%".format(precision * 100))
    print("The F1-score is: {:.1f}%".format(f1 * 100))