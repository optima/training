# Training log
The syntax below is all based on the default training procedure with yolov5:
- cd optima/training/yolov5
- conda activate optima
- python train.py --img 640 --batch 24 --epochs 300 --data apple_scab.yaml --weights weights/yolov5s.pt --name apple_scab_20200921_yolov5s <br/> <br/> 

**Please note that the W-drive below is the //wurnet.nl/dfs-root/projects/visionroboticsdata folder** <br/> <br/> 

The syntax below was used to train the yolov5 algorithms (in alphabetical order): 

**alternaria_20210712_yolov5s.pt**
| Parameter        		| Value		           								|
|:------------------------------|:--------------------------------------------------------------------------------------|
| --img				| 640											|
| --batch	    		| 24											|
| --epochs	 		| 300											|
| --data	 		| alternaria.yaml		 							|
| --weights			| W-drive/OPTIMA/YOLOv5/EDS/weights/yolov5s.pt				 		|
| --name			| alternaria_20210712_yolov5s						 		|
| dataset	 		| W-drive/OPTIMA/YOLOv5/EDS/alternaria_20210712						|

<br/> <br/>**alternaria_20211109_yolov5s.pt**
| Parameter        		| Value		           								|
|:------------------------------|:--------------------------------------------------------------------------------------|
| --img				| 640											|
| --batch	    		| 24											|
| --epochs	 		| 300											|
| --data	 		| alternaria.yaml		 							|
| --weights			| W-drive/OPTIMA/YOLOv5/EDS/weights/alternaria_20210712/weights/best.pt 		|
| --name			| alternaria_20211109_yolov5s						 		|
| dataset	 		| W-drive/OPTIMA/YOLOv5/EDS/alternaria_20211109						|

<br/> <br/>**apple_scab_20200921_yolov5s.pt**
| Parameter        		| Value		           								|
|:------------------------------|:--------------------------------------------------------------------------------------|
| --img				| 640											|
| --batch	    		| 24											|
| --epochs	 		| 300											|
| --data	 		| apple_scab.yaml		 							|
| --weights			| W-drive/OPTIMA/YOLOv5/EDS/weights/yolov5s.pt					 	|
| --name			| apple_scab_20200921_yolov5s						 		|
| dataset	 		| W-drive/OPTIMA/YOLOv5/EDS/apple_scab_20200921 					|

<br/> <br/>**apple_scab_20210401_yolov5s.pt**
| Parameter        		| Value		           								|
|:------------------------------|:--------------------------------------------------------------------------------------|
| --img				| 640											|
| --batch	    		| 24											|
| --epochs	 		| 300											|
| --data	 		| apple_scab.yaml		 							|
| --weights			| W-drive/OPTIMA/YOLOv5/EDS/weights/yolov5s.pt					 	|
| --name			| apple_scab_20210401_yolov5s						 		|
| dataset	 		| W-drive/OPTIMA/YOLOv5/EDS/apple_scab_20210401_yolov5s					|

<br/> <br/>**apple_scab_20210527_yolov5s.pt**
| Parameter        		| Value		           								|
|:------------------------------|:--------------------------------------------------------------------------------------|
| --img				| 640											|
| --batch	    		| 24											|
| --epochs	 		| 300											|
| --data	 		| apple_scab.yaml		 							|
| --weights			| W-drive/OPTIMA/YOLOv5/EDS/weights/apple_scab_20210401_yolov5s/weights/best.pt 	|
| --name			| apple_scab_20210527_yolov5s						 		|
| dataset	 		| W-drive/OPTIMA/YOLOv5/EDS/apple_scab_20210527						|

<br/> <br/>**apple_scab_20210527_finetune_yolov5s**
| Parameter        		| Value		           								|
|:------------------------------|:--------------------------------------------------------------------------------------|
| --img				| 640											|
| --batch	    		| 24											|
| --epochs	 		| 300											|
| --data	 		| apple_scab.yaml		 							|
| --weights			| W-drive/OPTIMA/YOLOv5/EDS/weights/apple_scab_20210401_yolov5s/weights/best.pt		|
| --name			| apple_scab_20210527_finetune_yolov5s					 		|
| --hyp 			| hyp.finetune.yaml									| 
| dataset	 		| W-drive/OPTIMA/YOLOv5/EDS/apple_scab_20210527						|

<br/> <br/>**apple_scab_20210610_yolov5s.pt**
| Parameter        		| Value		           								|
|:------------------------------|:--------------------------------------------------------------------------------------|
| --img				| 640											|
| --batch	    		| 24											|
| --epochs	 		| 300											|
| --data	 		| apple_scab.yaml		 							|
| --weights			| W-drive/OPTIMA/YOLOv5/EDS/weights/apple_scab_20210401_yolov5s/weights/best.pt 	|
| --name			| apple_scab_20210610_yolov5s						 		|
| dataset	 		| W-drive/OPTIMA/YOLOv5/EDS/apple_scab_20210610						|

<br/> <br/>**downy_mildew_20210310_yolov5s.pt**
| Parameter        		| Value		           								|
|:------------------------------|:--------------------------------------------------------------------------------------|
| --img				| 640											|
| --batch	    		| 24											|
| --epochs	 		| 300											|
| --data	 		| downy_mildew.yaml		 							|
| --weights			| W-drive/OPTIMA/YOLOv5/EDS/weights/yolov5s.pt					 	|
| --name			| downy_mildew_20210310_yolov5s						 		|
| dataset	 		| W-drive/OPTIMA/YOLOv5/EDS/downy_mildew_20210310					|

<br/> <br/>**downy_mildew_20210409_yolov5s.pt**
| Parameter        		| Value		           								|
|:------------------------------|:--------------------------------------------------------------------------------------|
| --img				| 640											|
| --batch	    		| 24											|
| --epochs	 		| 300											|
| --data	 		| downy_mildew.yaml		 							|
| --weights			| W-drive/OPTIMA/YOLOv5/EDS/weights/yolov5s.pt					 	|
| --name			| downy_mildew_20210409_yolov5s						 		|
| dataset	 		| W-drive/OPTIMA/YOLOv5/EDS/downy_mildew_20210409					|

<br/> <br/>**downy_mildew_20211224_yolov5s.pt**
| Parameter        		| Value		           								|
|:------------------------------|:--------------------------------------------------------------------------------------|
| --img				| 640											|
| --batch	    		| 24											|
| --epochs	 		| 300											|
| --data	 		| downy_mildew.yaml		 							|
| --weights			| W-drive/OPTIMA/YOLOv5/EDS/weights/downy_mildew_20210409/weights/best.pt 		|
| --name			| downy_mildew_20211224_yolov5s						 		|
| dataset	 		| W-drive/OPTIMA/YOLOv5/EDS/downy_mildew_20211224					|
