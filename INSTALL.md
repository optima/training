## Installation

## 1.) Install CUDA 10.2 (if not yet installed) 
Tested on: Ubuntu 18.04, CUDA: 10.2 (10.2.89)<br/> 

**1.1) Install NVIDIA drivers (using the terminal):** 
- sudo add-apt-repository ppa:graphics-drivers/ppa
- sudo apt update
- sudo apt install nvidia-driver-440
- nvidia-smi
- sudo reboot <br/> <br/>

**1.2) Download and install CUDA 10.2:** 
- wget https://developer.download.nvidia.com/compute/cuda/repos/ubuntu1804/x86_64/cuda-ubuntu1804.pin
- sudo mv cuda-ubuntu1804.pin /etc/apt/preferences.d/cuda-repository-pin-600
- wget https://developer.download.nvidia.com/compute/cuda/10.2/Prod/local_installers/cuda-repo-ubuntu1804-10-2-local-10.2.89-440.33.01_1.0-1_amd64.deb
- sudo dpkg -i cuda-repo-ubuntu1804-10-2-local-10.2.89-440.33.01_1.0-1_amd64.deb
- sudo apt-key add /var/cuda-repo-10-2-local-10.2.89-440.33.01/7fa2af80.pub
- sudo apt-get update
- sudo apt-get -y install cuda <br/> <br/>

**1.3) Set the environmental path:**
- sudo gedit ~/.bashrc
- add the following 2 lines at the end of the bashrc file:
export PATH=/usr/local/cuda-10.2/bin${PATH:+:${PATH}}
export LD_LIBRARY_PATH=/usr/local/cuda-10.2/lib64${LD_LIBRARY_PATH:+:${LD_LIBRARY_PATH}}
- save the bashrc file and close it
- source ~/.bashrc <br/> <br/>

**1.4) Check if CUDA has been installed properly:**
- nvcc --version *(should the CUDA details)*<br/> <br/>


## 2.) Install optima in a virtual environment (using Anaconda)
Tested with: Pytorch 1.7.0 and torchvision 0.8.1<br/>

**2.1) Download and install Anaconda:**
- download anaconda: https://www.anaconda.com/distribution/#download-section (python 3.x version)
- install anaconda (using the terminal, cd to the directory where the file has been downloaded): bash Anaconda3-2019.10-Linux-x86_64.sh <br/> <br/>

**2.2) Make a virtual environment (called optima) using the terminal:**
- conda create --name optima python=3.6 pip
- conda activate optima <br/> <br/>

**2.3) Download the code repository:**
- git clone https://git.wur.nl/optima/training.git optima/training
- cd optima/training <br/> <br/>

**2.4) Install the required software libraries (in the optima virtual environment, using the terminal):**
- pip install --upgrade pip
- pip install -U torch==1.7.0 torchvision==0.8.1 -f https://download.pytorch.org/whl/cu102/torch_stable.html
- pip install Cython
- pip install opencv-python
- pip install tqdm
- pip install matplotlib==3.2.2
- pip install pycocotools
- pip install pandas 
- pip install tensorboard==2.2
- pip install PyYAML==5.3.1
- pip install scipy==1.4.1
- pip install seaborn==0.11.0
- pip install onnx==1.8.1
- pip install coremltools==4.1
- pip install scikit-learn==0.19.2
- pip install onnxruntime-gpu==1.6.0
- pip install thop
- pip install transformers
- pip install jupyter
- pip install efficientnet_pytorch <br/> <br/>

**2.5) Reboot/restart the computer (sudo reboot)** <br/> <br/>

**2.6) Check if Pytorch links with CUDA (in the optima virtual environment, using the terminal):**
- python
- import torch
- torch.version.cuda *(should print 10.2)*
- torch.cuda.is_available() *(should True)*
- torch.cuda.get_device_name(0) *(should print the name of the first GPU)*
- quit() <br/> <br/>

## 3.) OPTIONAL: install TensorRT optimization (root environment, not in Anaconda environment)
Tested with: TensorRT version 7.2.2.3-1, CUDA 10.2, Ubuntu 18.04<br/>

**3.1) Download TensorRT:**
- Download TensorRT from NVIDIA-website: https://developer.nvidia.com/nvidia-tensorrt-download <br/> <br/>

**3.2) Install TensorRT:**
- cd
- conda deactivate
- sudo dpkg -i nv-tensorrt-repo-ubuntu1804-cuda10.2-trt7.2.2.3-ga-20201211_1-1_amd64.deb
- sudo apt-key add /var/nv-tensorrt-repo-cuda10.2-trt7.2.2.3-ga-20201211/7fa2af80.pub
- sudo apt-get update
- sudo apt-get install tensorrt
- sudo apt-get install python3-libnvinfer-dev
- sudo apt-get install onnx-graphsurgeon
- pip install pycuda
- dpkg -l | grep TensorRT <br/> <br/>

**3.3) Add TensorRT to the default Python environment (3.6):**
- sudo gedit ~/.bashrc
- export PYTHONPATH=/usr/lib/python3.6/dist-packages:$PYTHONPATH
- source ~/.bashrc <br/> <br/>
**Important:** if you want to use TensorRT in your virtual environment use the Python3.6 version! <br/> <br/>

**3.4) OPTIONAL: install ONNX-TensorRT (in the optima virtual environment, using the terminal):**
- cd optima/training
- conda activate optima
- pip install pycuda
- download ONNX-TensorRT that matches your TensorRT version: https://github.com/onnx/onnx-tensorrt/releases
- git clone https://github.com/onnx/onnx-tensorrt.git *(or different version)*
- cd onnx-tensorrt
- python setup.py install <br/> <br/>

**3.5) OPTIONAL: check ONNX-TensorRT (in the optima virtual environment, using the terminal):**
- python
- import onnx
- import onnx_tensorrt.backend as backend *(should not print an error)*
- quit() <br/> <br/>
